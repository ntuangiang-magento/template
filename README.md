![Docker Stars](https://img.shields.io/docker/stars/ntuangiang/magento-nginx.svg)
![Docker Pulls](https://img.shields.io/docker/pulls/ntuangiang/magento-nginx.svg)
![Docker Automated build](https://img.shields.io/docker/automated/ntuangiang/magento-nginx.svg)

# Magento 2 Template
[https://devdocs.magento.com](https://devdocs.magento.com) Meet the small business, mid-sized business, and enterprise-level companies who are benefiting from the power and flexibility of Magento on their web stores. We built the eCommerce platform, so you can build your business.

## Usage
- Build Images

```shell
docker build -t magento-nginx:2.4.0 . --target=magento-nginx
docker build -t magento:2.4.0 . --target=magento-php-fpm
```

- Start Services

```yml
docker-compose up 
```
 - Or
 
 ```yml
 docker-compose up -d 
 ```

```
Showing the 2FA screen
Add var_dump($url);die; on line 86 in vendor/magento/module-two-factor-auth/Model/EmailUserNotifier.php
Refresh /admin*
Copy the URL that is displayed
Remove the var_dump
Use the URL copied earlier to navigate to the 2FA setup
```
## LICENSE

MIT License
