FROM ntuangiang/magento-cache:2.4.0 as magento-build

# Remember docker image prune after build successfuly.

# Magento Mode
ENV MAGENTO_MODE=production

# Host
ENV MAGENTO_SEARCH_ENGINE_HOST=host.docker.internal
ENV MAGENTO_CACHE_REDIS_HOST=host.docker.internal
ENV MAGENTO_DATABASE_HOST=host.docker.internal

# Database Configuration
ENV MAGENTO_DATABASE_HOST=host.docker.internal
ENV MAGENTO_DATABASE_USER=magento2
ENV MAGENTO_DATABASE_PWD=magento2
ENV MAGENTO_DATABASE_DB=magento2

# Magento Setup
ENV MAGENTO_EXPORT_DB=true
ENV MAGENTO_ADMIN_PWD=magento2
ENV MAGENTO_ADMIN_EMAIL=gnt.v37@gmail.com

# Cache
ENV MAGENTO_CACHE_REDIS_HOST=m2redis
ENV MAGENTO_SEARCH_ENGINE_HOST=host.docker.internal

# Url
ENV MAGENTO_BASE_URL=http://magento2.dev.traefik
ENV MAGENTO_BASE_URL_SECURE=https://magento2.dev.traefik

COPY ./composer.* ${DOCUMENT_ROOT}/

RUN magento:install

COPY ./app/code ${DOCUMENT_ROOT}/app/code

RUN magento:setup

# --- PHP FPM ---
FROM ntuangiang/magento:2.4.0 as magento-php-fpm

COPY --from=magento-build --chown=magento:magento \
    ${DOCUMENT_ROOT}/ \
    ${DOCUMENT_ROOT}/

COPY --chown=magento:magento ./app/etc/env.php.template ./app/etc/env.php

# --- NGINX SERVER ---
FROM ntuangiang/magento-nginx as magento-nginx

COPY --from=magento-php-fpm --chown=magento:magento \
    ${DOCUMENT_ROOT}/ \
    ${DOCUMENT_ROOT}/

COPY --chown=magento:magento ./app/etc/env.php.template ./app/etc/env.php
